const mongoose = require("mongoose")

const Comments = mongoose.Schema({
      Author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
            required: true
      },
      Posts: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Posts",
            required: true
      },
      text: {
            type: String,
            required: true
      }
})


module.exports = mongoose.model("Comments", Comments)