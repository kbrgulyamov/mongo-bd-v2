const express = require("express")
const router = express.Router()
const Comments = require('../models/Comments')

router.get("/", (req, res) => {
      try {
            let comments = Comments.find().populate("usersId")

            res.json({
                  ok: true,
                  message: 'Posts получены',
                  data: comments
            })
      }
      catch (error) {
            console.log(error);
      }
      console.log("Get all posts! :)");
})

// router.get("/:id", async (req, res) => {
//       console.log('Get element: ' + req.params.id);

//       try {
//             let comm = await Comments
//                   .findById(req.params.id)
//                   .populate("usersId")

//             Posts.findOneAndUpdate({ _id: req.params.id }, { $inc: { numShow: 1 } }, function (err, response) {
//                   if (err) {
//                         console.log(err);
//                   } else {
//                         console.log(response);
//                   }
//             })

//             res.json({
//                   ok: true,
//                   message: "Element found",
//                   data: prod
//             })
//       }
//       catch (error) {
//             console.log(error);
//       }
// })

router.patch("/:id", async (req, res) => {
      try {
            Comments.findByIdAndUpdate(req.params.id, req.body, (error, data) => {
                  if (error) {
                        console.log(error);

                        res.json({
                              ok: false,
                              message: "Error inside callback function WTF!",
                              error
                        })
                  } else {
                        res.json({
                              ok: true,
                              message: "Element patch!",
                              element: data
                        })
                  }
            })
      }
      catch (error) {
            res.json({
                  ok: false,
                  message: "Seems there nogoods!",
                  error
            })
      }
})

router.post("/", async (req, res) => {
      try {
            let posts = await Comments.find().populate("usersId")

            Comments.create(req.body, async (error, data) => {
                  if (error) {
                        console.log(error);

                        res.json({
                              ok: false,
                              message: "Error inside callback function WTF!",
                              error
                        })
                  } else {
                        let user = await Users.findById(data.usersId)

                        user.comments.push(data.id)
                        user.productsCount = user.products.length
                        user.save()

                        res.json({
                              ok: true,
                              message: "Element created!",
                              element: posts
                        })
                  }
            })
      } catch (error) {
            console.log(error);

            res.json({
                  ok: false,
                  message: "Some error",
                  error
            })
      }
})

router.delete("/:id", async (req, res) => {
      Comments.findByIdAndDelete(req.params.id, async (error, data) => {
            if (error) {
                  res.json({
                        ok: false,
                        messege: "Deleted shit!",
                        el: data,
                        error
                  })
            } else {
                  res.json({
                        ok: true,
                        message: 'Deleted',
                        element: data,
                  })
            }
      })
})

module.exports = router